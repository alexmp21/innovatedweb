import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-micuenta',
  templateUrl: './micuenta.component.html',
  styles: [
  ]
})
export class MicuentaComponent implements OnInit {

  public cambiarcontra:boolean;
  public guardar:boolean;
  constructor() {
    this.cambiarcontra = false;
    this.guardar =false;
   }


  ngOnInit(): void {
  }

  onCambiar(){
    this.cambiarcontra = true;
  }

  onGuardar(){
    this.cambiarcontra = false;
  }

}
