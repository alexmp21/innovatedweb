import { NgModule } from '@angular/core';
import {  RouterModule, Routes } from '@angular/router'
import { BienvenidafactesolComponent } from './pages/bienvenidafactesol/bienvenidafactesol.component';
import { BienvenidafactutedComponent } from './pages/bienvenidafactuted/bienvenidafactuted.component';
import { CertificadoComponent } from './pages/certificado/certificado.component';
import { DocumentosComponent } from './pages/documentos/documentos.component';
import { FactesolComponent } from './pages/factesol/factesol.component';
import { FactutedComponent } from './pages/factuted/factuted.component';
import { HomeComponent } from './pages/home/home.component';
import { InicioComponent } from './pages/inicio/inicio.component';
import { MicuentaComponent } from './pages/micuenta/micuenta.component';
import { MisdocumentoComponent } from './pages/misdocumento/misdocumento.component';
import { SeleccionerubrofactutedComponent } from './pages/seleccionerubrofactuted/seleccionerubrofactuted.component';

export const rutas:Routes = [
  
  {path:'home',component:HomeComponent},
  {path:'factesol',component:FactesolComponent},
  {path:'factuted',component:FactutedComponent},
  {path:'inicio', component:InicioComponent},
  {path:'documentos',component:DocumentosComponent},
  {path:'misdocumentos',component:MisdocumentoComponent},
  {path:'certificado',component:CertificadoComponent},
  {path:'bienvenidafactesol',component:BienvenidafactesolComponent},
  {path:'bienvenidafactuted',component:BienvenidafactutedComponent},
  {path:'seleccionerubro', component:SeleccionerubrofactutedComponent},
  {path:'micuenta', component:MicuentaComponent},
  {path:'**',pathMatch:'full',redirectTo:'home'}
]


@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(rutas)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
