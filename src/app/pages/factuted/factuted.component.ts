import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-factuted',
  templateUrl: './factuted.component.html',
  styles: [
  ]
})
export class FactutedComponent implements OnInit {

  public mostrarDatos:boolean;


  constructor() {
    this.mostrarDatos = false;
   }

  ngOnInit(): void {
  }

  onShowHide(value){
    this.mostrarDatos = value;
  }

}
