import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ComponentsModule } from '../components/components.module';
import { FactesolComponent } from './factesol/factesol.component';
import { AppRoutingModule } from '../app-routing.module';
import { FactutedComponent } from './factuted/factuted.component';
import { InicioComponent } from './inicio/inicio.component';
import { DocumentosComponent } from './documentos/documentos.component';
import { CertificadoComponent } from './certificado/certificado.component';
import { BienvenidafactesolComponent } from './bienvenidafactesol/bienvenidafactesol.component';
import { BienvenidafactutedComponent } from './bienvenidafactuted/bienvenidafactuted.component';
import { MisdocumentoComponent } from './misdocumento/misdocumento.component';
import { SeleccionerubrofactutedComponent } from './seleccionerubrofactuted/seleccionerubrofactuted.component';
import { MicuentaComponent } from './micuenta/micuenta.component';

@NgModule({
  declarations: [HomeComponent, FactesolComponent, FactutedComponent, InicioComponent, DocumentosComponent, CertificadoComponent, BienvenidafactesolComponent, BienvenidafactutedComponent, MisdocumentoComponent, SeleccionerubrofactutedComponent, MicuentaComponent, ],
  imports: [
    CommonModule,
    ComponentsModule,
    AppRoutingModule
  ]
})
export class PagesModule { }
