import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-misdocumento',
  templateUrl: './misdocumento.component.html',
  styles: [
  ]
})
export class MisdocumentoComponent implements OnInit {

  public elegirCotizar:boolean;
  public elegirContrato:boolean;
  public mostarDatos:boolean;
  public elegir:boolean;
  constructor() { 
    this.mostarDatos = false;
    this.elegirCotizar = false;
    this.elegirContrato = false;
    this.elegir = false;
  }

  ngOnInit(): void {
  }

  onShowHide(){
    this.mostarDatos =true; 
  }
  onCotizar(){
    this.elegirCotizar =true;
    this.elegirContrato =false;
    this.elegir = true;
  }
  onContrato(){
    this.elegirContrato =true;
    this.elegirCotizar =false;
    this.elegir = true;
  }

}
