import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-certificado',
  templateUrl: './certificado.component.html',
  styles: [
  ]
})
export class CertificadoComponent implements OnInit {

  public solicitarcertificado:boolean;
  public solicitudenviada:boolean;


  constructor() {
    this.solicitarcertificado = false;
    this.solicitudenviada = false;
   }

  ngOnInit(): void {
  }

  onSolicitar(){
    this.solicitarcertificado = true;
  }

  onEnviada(){
    this.solicitudenviada = true;
    
  }


}
