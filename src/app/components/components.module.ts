import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { CarouselComponent } from './carousel/carousel.component';
import { AppRoutingModule } from '../app-routing.module';



@NgModule({
  declarations: [NavbarComponent, FooterComponent, CarouselComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    
  ],
  exports:[
    NavbarComponent,
    FooterComponent,
    CarouselComponent
  ]
})
export class ComponentsModule { }
