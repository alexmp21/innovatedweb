import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.component.html',
  styles: [
  ]
})
export class DocumentosComponent implements OnInit {

  public elegirCotizar:boolean;
  public elegirContrato:boolean;
  public mostarDatos:boolean;
  public elegir:boolean;
  public desaparecer:boolean;
  public elegirvolver:boolean;
  public eliminar:boolean;
  
  constructor() {
    this.mostarDatos = false;
    this.elegirCotizar = false;
    this.elegirContrato = false;
    this.elegir = false;
    this.desaparecer = false;
    this.elegirvolver = false;
    this.eliminar = false;
   }

  ngOnInit(): void {
  }

  onShowHide(){
    this.mostarDatos =true; 
  }
  onCotizar(){
    this.elegirCotizar =true;
    this.elegirContrato =false;
    this.elegir = true;
  }
  onContrato(){
    this.elegirContrato =true;
    this.elegirCotizar =false;
    this.elegir = true;
  }
  onDesaparecer(){
    this.desaparecer = true;
  }
  
  onVolver(){
    this.desaparecer = false;
    this.eliminar = true;
  }
}
